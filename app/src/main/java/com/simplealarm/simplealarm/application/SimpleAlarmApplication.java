package com.simplealarm.simplealarm.application;

import android.app.Application;

import com.orm.SugarContext;

/**
 * Created by DEV on 2/18/2017.
 */

public class SimpleAlarmApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        SugarContext.init(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        SugarContext.terminate();
    }
}
