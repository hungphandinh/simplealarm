package com.simplealarm.simplealarm;

import android.app.Activity;

/**
 * Created by DEV on 2/19/2017.
 */

public interface Constants {
    int REQUEST_ADD_ALARM = 1;
    int RESULT_OK = Activity.RESULT_OK;
    int RESULT_CANCELED = Activity.RESULT_CANCELED;
}
