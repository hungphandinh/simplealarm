package com.simplealarm.simplealarm.screen.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.simplealarm.simplealarm.data.Alarm;

import java.util.List;

/**
 * Created by DEV on 2/19/2017.
 */

public class MainFragment extends Fragment implements MainScreenContract.View {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void showAlarms(List<Alarm> alarms) {

    }

    @Override
    public void showNoAlarm() {

    }

    @Override
    public void showDeleteSuccess() {

    }

    @Override
    public void showAddAlarm() {

    }

    @Override
    public void showAddTimer() {

    }

    @Override
    public void showEditeAlarm(@NonNull Alarm alarm) {

    }

    @Override
    public void isActive() {

    }

    @Override
    public void setLoadingIndicator(boolean show) {

    }

    @Override
    public void setPresenter(MainScreenContract.Presenter presenter) {

    }
}
