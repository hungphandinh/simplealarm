package com.simplealarm.simplealarm.screen.main;

import android.support.annotation.NonNull;

import com.simplealarm.simplealarm.BasePresenter;
import com.simplealarm.simplealarm.BaseView;
import com.simplealarm.simplealarm.data.Alarm;

import java.util.List;

/**
 * Created by DEV on 2/19/2017.
 */

public interface MainScreenContract {

    interface Presenter extends BasePresenter {
        void result(int requestCode, int resultCode);

        void loadAlarms(boolean forceUpdate);

        void addNewAlarm();

        void addNewTimer();

        void openAlarmEdit(@NonNull Alarm alarm);

        void activeAlarm(@NonNull Alarm alarm);

        void inActiveAlarm(@NonNull Alarm alarm);

        void activeTimer(@NonNull Alarm alarm);

        void inactiveTimer(@NonNull Alarm alarm);

        void deleteAlarm(@NonNull Alarm alarm);

    }

    interface View extends BaseView<Presenter> {

        void showAlarms(List<Alarm> alarms);

        void showNoAlarm();

        void showDeleteSuccess();

        void showAddAlarm();

        void showAddTimer();

        void showEditeAlarm(@NonNull Alarm alarm);

        void isActive();

        void setLoadingIndicator(boolean show);
    }

}
