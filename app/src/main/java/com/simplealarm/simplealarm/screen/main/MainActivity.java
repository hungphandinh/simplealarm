package com.simplealarm.simplealarm.screen.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.simplealarm.simplealarm.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
