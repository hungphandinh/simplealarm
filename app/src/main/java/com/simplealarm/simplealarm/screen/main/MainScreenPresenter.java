package com.simplealarm.simplealarm.screen.main;

import android.support.annotation.NonNull;

import com.simplealarm.simplealarm.Constants;
import com.simplealarm.simplealarm.data.Alarm;
import com.simplealarm.simplealarm.data.AlarmDataStorage;

import java.util.List;

/**
 * Created by DEV on 2/19/2017.
 */

public class MainScreenPresenter implements MainScreenContract.Presenter {

    private final AlarmDataStorage mAlarmDataStorage;
    private final MainScreenContract.View mView;

    public MainScreenPresenter(@NonNull AlarmDataStorage mAlarmDataStorage, @NonNull MainScreenContract.View view) {
        this.mAlarmDataStorage = mAlarmDataStorage;
        this.mView = view;
    }

    @Override
    public void result(int requestCode, int resultCode) {
        if (requestCode == Constants.REQUEST_ADD_ALARM && resultCode == Constants.RESULT_OK) {
            loadAlarms(true);
        }
    }

    @Override
    public void loadAlarms(boolean forceUpdate) {
        mView.setLoadingIndicator(true);
        if (forceUpdate) {
            mAlarmDataStorage.refreshAlarms();
        }
        mAlarmDataStorage.getAlarms(new AlarmDataStorage.GetAlarmsCallback() {
            @Override
            public void onGetAlarms(List<Alarm> alarms) {
                mView.showAlarms(alarms);
                mView.setLoadingIndicator(false);
            }

            @Override
            public void onDataNotAvailable() {
                mView.showNoAlarm();
                mView.setLoadingIndicator(false);
            }
        });
    }

    @Override
    public void addNewAlarm() {
        mView.showAddAlarm();
    }

    @Override
    public void addNewTimer() {
        mView.showAddTimer();
    }

    @Override
    public void openAlarmEdit(Alarm alarm) {
        mView.showEditeAlarm(alarm);
    }

    @Override
    public void activeAlarm(@NonNull Alarm alarm) {
        mAlarmDataStorage.activeAlarm(alarm);
        loadAlarms(false);
    }

    @Override
    public void inActiveAlarm(@NonNull Alarm alarm) {
        mAlarmDataStorage.inActiveAlarm(alarm);
        loadAlarms(false);
    }

    @Override
    public void activeTimer(@NonNull Alarm alarm) {
        activeAlarm(alarm);
    }

    @Override
    public void inactiveTimer(@NonNull Alarm alarm) {
        inActiveAlarm(alarm);
    }

    @Override
    public void deleteAlarm(@NonNull Alarm alarm) {
        mAlarmDataStorage.deleteAlarm(alarm);
        loadAlarms(false);
    }

    @Override
    public void start() {
        loadAlarms(true);
    }
}
