package com.simplealarm.simplealarm.screen.timeup;

import com.simplealarm.simplealarm.BasePresenter;
import com.simplealarm.simplealarm.BaseView;

/**
 * Created by DEV on 2/19/2017.
 */

public interface TimeUpScreenContract {

    interface Presenter extends BasePresenter {

        void loadAlarm(Long id);

        void setAlarmComplete();

        void snozze();
    }

    interface View extends BaseView<Presenter> {

        void showAlarmLabel(String label);

        void dismiss();
    }


}
