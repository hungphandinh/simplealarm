package com.simplealarm.simplealarm.screen.addeditalarm;

import com.simplealarm.simplealarm.BasePresenter;
import com.simplealarm.simplealarm.BaseView;
import com.simplealarm.simplealarm.data.Alarm;

/**
 * Created by DEV on 2/19/2017.
 */

public interface AddEditContract {

    interface Presenter extends BasePresenter {
        void saveAlarm(Alarm alarm);
    }

    interface View extends BaseView<Presenter> {

        void showNewAlarm();

        void showAddSuccess();

        void showDeleteSucess();

        void showEditSucess();

        void setAlarmTime(Long time);

        void setVibrate(boolean vibrate);

        void setRingResource(String resource);

        void setRepeat(String repeat);

        void showListAlarm();

    }

}
