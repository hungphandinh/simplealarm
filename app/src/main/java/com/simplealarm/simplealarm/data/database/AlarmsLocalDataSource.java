package com.simplealarm.simplealarm.data.database;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.orm.SugarRecord;
import com.simplealarm.simplealarm.data.Alarm;
import com.simplealarm.simplealarm.data.AlarmDataStorage;

import java.util.List;

/**
 * Created by DEV on 2/18/2017.
 */

public class AlarmsLocalDataSource implements AlarmDataStorage {

    interface ActionInBackground<T> {
        T action();
    }

    interface OnBackgroundCallback<T> {
        void onCallback(T t);
    }

    private <T> void executeDataAction(final ActionInBackground<T> actionInBackground, final OnBackgroundCallback<T> backgroundCallback) {
        AsyncTask<ActionInBackground<T>, Void, T> asyncTask = new AsyncTask<ActionInBackground<T>, Void, T>() {
            @Override
            protected T doInBackground(ActionInBackground<T>... params) {
                return params[0].action();
            }

            @Override
            protected void onPostExecute(T t) {
                super.onPostExecute(t);
                backgroundCallback.onCallback(t);
            }
        };
        asyncTask.execute(actionInBackground);
    }

    @Override
    public void getAlarms(@NonNull final GetAlarmsCallback getAlarmsCallback) {
        ActionInBackground<List<Alarm>> listActionInBackgroud = () -> SugarRecord.listAll(Alarm.class);
        OnBackgroundCallback<List<Alarm>> listOnBackgroundCallback = getAlarmsCallback::onGetAlarms;

        executeDataAction(listActionInBackgroud, listOnBackgroundCallback);
    }

    @Override
    public void getAlarm(@NonNull Long id, @NonNull GetAlarmCallback getAlarmCallback) {
        ActionInBackground<Alarm> getAlarm = () -> SugarRecord.findById(Alarm.class, id);
        OnBackgroundCallback<Alarm> callback = getAlarmCallback::onGetAlarm;

        executeDataAction(getAlarm, callback);
    }

    @Override
    public void saveAlarm(@NonNull Alarm alarm) {
        SugarRecord.save(alarm);
    }

    @Override
    public void completeAlarm(@NonNull Alarm alarm) {
        alarm.setComplete(true);
    }

    @Override
    public void activeAlarm(@NonNull Alarm alarm) {
        alarm.setActive(true);
    }

    @Override
    public void inActiveAlarm(@NonNull Alarm alarm) {
        alarm.setActive(false);
    }

    @Override
    public void clearCompleteAlarms() {
        SugarRecord.deleteAll(Alarm.class, AlarmPersisteneContract.IS_COMPLETE, "true");
    }

    @Override
    public void refreshAlarms() {
        // not implement here
    }

    @Override
    public void deleteAllAlarms() {
        SugarRecord.deleteAll(Alarm.class);
    }

    @Override
    public void deleteAlarm(@NonNull Alarm alarm) {
        Alarm needDeleteAlarm = SugarRecord.findById(Alarm.class, alarm.getId());
        SugarRecord.delete(needDeleteAlarm);
    }
}
