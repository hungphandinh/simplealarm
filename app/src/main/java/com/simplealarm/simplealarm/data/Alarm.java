package com.simplealarm.simplealarm.data;

import com.orm.dsl.Table;

/**
 * Created by DEV on 2/18/2017.
 */

@Table
public class Alarm {

    private final Long id;
    private String label;
    private long timeInMilis;
    private boolean isVibrate;
    private boolean isRing;
    private String ringSource;
    private boolean isComplete;
    private boolean isActive;
    private boolean[] loopInWeek;
    private boolean isLoop;

    public Alarm(Long id) {
        this.id = id;
    }

    public boolean isLoop() {
        return isLoop;
    }

    public void setLoop(boolean loop) {
        isLoop = loop;
    }

    public boolean[] getLoopInWeek() {
        return loopInWeek;
    }

    public void setLoopInWeek(boolean[] loopInWeek) {
        this.loopInWeek = loopInWeek;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public Long getId() {
        return id;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public long getTimeInMilis() {
        return timeInMilis;
    }

    public void setTimeInMilis(long timeInMilis) {
        this.timeInMilis = timeInMilis;
    }

    public boolean isVibrate() {
        return isVibrate;
    }

    public void setVibrate(boolean vibrate) {
        isVibrate = vibrate;
    }

    public boolean isRing() {
        return isRing;
    }

    public void setRing(boolean ring) {
        isRing = ring;
    }

    public String getRingSource() {
        return ringSource;
    }

    public void setRingSource(String ringSource) {
        this.ringSource = ringSource;
    }
}
