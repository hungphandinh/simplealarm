package com.simplealarm.simplealarm.data;

import android.support.annotation.NonNull;

import com.google.common.base.Preconditions;
import com.simplealarm.simplealarm.data.cache.AlarmCacheDataSource;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by DEV on 2/19/2017.
 */

public class AlarmRepository implements AlarmDataStorage {

    private final AlarmDataStorage localAlarmDataStorage;
    private final AlarmDataStorage cacheAlarmDataStorage;

    private static AlarmRepository INSTANCE;

    private final Map<Long, Alarm> mCachedAlarms = new LinkedHashMap<>();

    private boolean mCacheIsDirty = false;

    private AlarmRepository(AlarmDataStorage localAlarmDataStorage) {
        this.localAlarmDataStorage = localAlarmDataStorage;
        this.cacheAlarmDataStorage = new AlarmCacheDataSource();
    }

    public synchronized static AlarmRepository getInstance(AlarmDataStorage localAlarmDataStorage) {
        if (INSTANCE == null) {
            INSTANCE = new AlarmRepository(localAlarmDataStorage);
        }
        return INSTANCE;
    }

    public synchronized static void destroyInstance() {
        INSTANCE = null;
    }

    @Override
    public void getAlarms(@NonNull GetAlarmsCallback getAlarmsCallback) {
        Preconditions.checkNotNull(getAlarmsCallback);

        if (!mCacheIsDirty) {
            getAlarmsCallback.onGetAlarms(new ArrayList<>(mCachedAlarms.values()));
        } else {
            getAlarmsFromDataSource(getAlarmsCallback);
        }
    }

    private void getAlarmsFromDataSource(GetAlarmsCallback getAlarmsCallback) {
        localAlarmDataStorage.getAlarms(new GetAlarmsCallback() {
            @Override
            public void onGetAlarms(List<Alarm> alarms) {
                refreshCache(alarms);
                getAlarmsCallback.onGetAlarms(alarms);
            }

            @Override
            public void onDataNotAvailable() {
                getAlarmsCallback.onDataNotAvailable();
            }
        });
    }

    private void refreshCache(List<Alarm> alarms) {
        mCachedAlarms.clear();
        for (Alarm alarm : alarms) {
            mCachedAlarms.put(alarm.getId(), alarm);
        }
        mCacheIsDirty = false;
    }

    @Override
    public void getAlarm(@NonNull Long id, @NonNull GetAlarmCallback getAlarmCallback) {
        Preconditions.checkNotNull(getAlarmCallback);
        if (!mCacheIsDirty && mCachedAlarms.get(id) != null) {
            getAlarmCallback.onGetAlarm(mCachedAlarms.get(id));
        } else {
            localAlarmDataStorage.getAlarm(id, new GetAlarmCallback() {
                @Override
                public void onGetAlarm(Alarm alarm) {
                    mCachedAlarms.put(alarm.getId(), alarm);
                    getAlarmCallback.onGetAlarm(alarm);
                }

                @Override
                public void onDataNotAvailable() {
                    getAlarmCallback.onDataNotAvailable();
                }
            });
        }
    }

    @Override
    public void saveAlarm(@NonNull Alarm alarm) {
        mCachedAlarms.put(alarm.getId(), alarm);
        localAlarmDataStorage.saveAlarm(alarm);
    }

    @Override
    public void completeAlarm(@NonNull Alarm alarm) {
        localAlarmDataStorage.completeAlarm(alarm);
        saveAlarm(alarm);
    }

    @Override
    public void activeAlarm(@NonNull Alarm alarm) {
        localAlarmDataStorage.activeAlarm(alarm);
        saveAlarm(alarm);
    }

    @Override
    public void inActiveAlarm(@NonNull Alarm alarm) {
        localAlarmDataStorage.inActiveAlarm(alarm);
        saveAlarm(alarm);
    }

    @Override
    public void clearCompleteAlarms() {
        Iterator<Map.Entry<Long, Alarm>> alarmIterator = mCachedAlarms.entrySet().iterator();
        while (alarmIterator.hasNext()) {
            if (alarmIterator.next().getValue().isComplete()) {
                alarmIterator.remove();
            }
        }
        localAlarmDataStorage.clearCompleteAlarms();
    }

    @Override
    public void refreshAlarms() {
        mCacheIsDirty = true;
    }

    @Override
    public void deleteAllAlarms() {
        mCachedAlarms.clear();
        localAlarmDataStorage.deleteAllAlarms();
    }

    @Override
    public void deleteAlarm(@NonNull Alarm alarm) {
        mCachedAlarms.remove(alarm);
        localAlarmDataStorage.deleteAlarm(alarm);
    }
}
