package com.simplealarm.simplealarm.data;

import android.support.annotation.NonNull;

import java.util.List;

/**
 * Created by DEV on 2/18/2017.
 */

public interface AlarmDataStorage {

    interface GetAlarmCallback {
        void onGetAlarm(Alarm alarm);

        void onDataNotAvailable();
    }

    interface GetAlarmsCallback {
        void onGetAlarms(List<Alarm> alarms);

        void onDataNotAvailable();
    }

    void getAlarms(@NonNull GetAlarmsCallback getAlarmsCallback);

    void getAlarm(@NonNull Long id, @NonNull GetAlarmCallback getAlarmCallback);

    void saveAlarm(@NonNull Alarm alarm);

    void completeAlarm(@NonNull Alarm alarm);

    void activeAlarm(@NonNull Alarm alarm);

    void inActiveAlarm(@NonNull Alarm alarm);

    void clearCompleteAlarms();

    void refreshAlarms();

    void deleteAllAlarms();

    void deleteAlarm(@NonNull Alarm alarm);

}
