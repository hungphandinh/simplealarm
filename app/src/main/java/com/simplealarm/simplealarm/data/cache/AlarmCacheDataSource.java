package com.simplealarm.simplealarm.data.cache;

import android.support.annotation.NonNull;

import com.simplealarm.simplealarm.data.Alarm;
import com.simplealarm.simplealarm.data.AlarmDataStorage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by DEV on 2/19/2017.
 */

public class AlarmCacheDataSource implements AlarmDataStorage {

    private final Map<Long, Alarm> mCacheAlarms;

    public AlarmCacheDataSource() {
        mCacheAlarms = new LinkedHashMap<>();
    }

    @Override
    public void getAlarms(@NonNull GetAlarmsCallback getAlarmsCallback) {
        List<Alarm> alarmList = new ArrayList<>(mCacheAlarms.values());
        if (!alarmList.isEmpty()) {
            getAlarmsCallback.onGetAlarms(alarmList);
        } else {
            getAlarmsCallback.onDataNotAvailable();
        }
    }

    @Override
    public void getAlarm(@NonNull Long id, @NonNull GetAlarmCallback getAlarmCallback) {
        Alarm alarm = mCacheAlarms.get(id);
        if (alarm != null) {
            getAlarmCallback.onGetAlarm(alarm);
        } else {
            getAlarmCallback.onDataNotAvailable();
        }
    }

    @Override
    public void saveAlarm(@NonNull Alarm alarm) {
        mCacheAlarms.put(alarm.getId(), alarm);
    }

    @Override
    public void completeAlarm(@NonNull Alarm alarm) {
        alarm.setComplete(true);
        saveAlarm(alarm);
    }

    @Override
    public void activeAlarm(@NonNull Alarm alarm) {
        alarm.setActive(true);
        saveAlarm(alarm);
    }

    @Override
    public void inActiveAlarm(@NonNull Alarm alarm) {
        alarm.setActive(false);
        saveAlarm(alarm);
    }

    @Override
    public void clearCompleteAlarms() {
        Iterator<Map.Entry<Long, Alarm>> alarmIterator = mCacheAlarms.entrySet().iterator();
        while (alarmIterator.hasNext()) {
            if (alarmIterator.next().getValue().isComplete()) {
                alarmIterator.remove();
            }
        }
    }

    @Override
    public void refreshAlarms() {
        // do no thing
    }

    @Override
    public void deleteAllAlarms() {
        mCacheAlarms.clear();
    }

    @Override
    public void deleteAlarm(@NonNull Alarm alarm) {
        mCacheAlarms.remove(alarm);
    }
}
