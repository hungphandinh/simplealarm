package com.simplealarm.simplealarm;

/**
 * Created by DEV on 2/19/2017.
 */

public interface BaseView<T> {

    void setPresenter(T presenter);

}
